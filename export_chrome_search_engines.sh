#!/bin/sh
#################################################################
# VARIABLES
# these variables will have to be changed depending on your device

# CHROME PROFILE FILE PATH
CHROMEPROFILE=~/Library/Application\ Support/Google/Chrome/Profile\ 2

# Options include
#~/Library/Application\ Support/Google/Chrome/Default
#~/Library/Application\ Support/Google/Chrome/Profile\ 1
#~/Library/Application\ Support/Google/Chrome/Profile\ 2
#~/Library/Application\ Support/Google/Chrome/Profile\ 3

# FILEPATH - Keywords.sql
DESTINATION=~/Scripts/Chrome-Search-Engine-Sync/keywords.sql
#DESTINATION=${1:-./keywords.sql}

#################################################################

TEMP_SQL_SCRIPT=/tmp/sync_chrome_sql_script
echo "Exporting Chrome keywords to $DESTINATION..."
cd "$CHROMEPROFILE"
echo .output $DESTINATION > $TEMP_SQL_SCRIPT
echo .dump keywords >> $TEMP_SQL_SCRIPT
sqlite3 -init $TEMP_SQL_SCRIPT Web\ Data .exit
rm $TEMP_SQL_SCRIPT
